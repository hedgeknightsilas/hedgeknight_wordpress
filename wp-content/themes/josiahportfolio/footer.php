<footer>

	<div class="padding-wrapper">
	  <div class="legal">
	  	<p>© <?php echo date("Y"); ?> <?php bloginfo( 'name' ); ?></p>
	  </div>

	  <div class="social">
  		
  		<?php if( have_rows( 'social', 'options' ) ): ?>

  			<?php while( have_rows( 'social', 'options' ) ): the_row(); ?>

  				<?php

            $image = get_sub_field( 'social_icon', 'options' );
            $size = 'icon';
            $src = $image['url'];
            $alt = $image['alt'];
            $thumb = $image['sizes'][ $size ];

          if( $image ): ?>

	  				<a href="<?php the_sub_field( 'social_link', 'options' ); ?>">

	  					<img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" />

	  				</a>

	  			<?php endif; ?>

  			<?php endwhile; ?>

  		<?php endif; ?>

  	</div>

	</div>

</footer>

<?php wp_footer(); ?>

</body>
</html>
