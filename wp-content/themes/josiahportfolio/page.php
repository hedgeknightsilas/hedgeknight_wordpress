<?php get_header(); ?>

  <?php if( get_field( 'page_intro' ) ): ?>

    <div class="page-intro">
      <div class="padding-wrapper">
        <div class="text-wrapper">

          <?php the_field( 'page_intro' ); ?>

        </div>
      </div>
    </div>

  <?php endif; ?>

  <main class="site-main subpage" role="main">

    <div class="padding-wrapper">

      <div class="text-wrapper">

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

          <div class="page-content clearfix">

            <?php the_content(); ?>
          </div>

        <?php endwhile; endif; ?>

      </div>

    </div>
  </main>

<?php get_footer(); ?>
