<?php /* Template Name: Work Examples */ ?>

<?php get_header(); ?>

<main class="site-main subpage" role="main">
  <div class="padding-wrapper">
    <div class="text-wrapper">

      <div class="main-wordpress-content">

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

          <?php the_content(); ?>

        <?php endwhile; endif; wp_reset_query(); ?>

      </div>

    </div>

    <div class="medium-wrapper">

      <div class="work-examples">

        <?php while( have_rows( 'work_examples', 'options' ) ): the_row(); ?>

          <div class="work-example">

            <a href="<?php the_sub_field( 'project_link', 'options' ); ?>">

              <?php

                $image = get_sub_field( 'image' );
                $size = 'medium-flexible';
                $src = $image['url'];
                $alt = $image['alt'];
                $thumb = $image['sizes'][ $size ];

              if( $image ): ?>

                <img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" />

              <?php endif; ?>

              <!-- <h3><?php the_sub_field( 'project_title' ); ?></h3>
 -->
            </a>

          </div>

        <?php endwhile; ?>

      </div>

    </div>
  </div>
</main>

<?php get_footer(); ?>
