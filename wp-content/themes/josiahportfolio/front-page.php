<?php get_header(); ?>

  <?php if( get_field( 'page_intro' ) ): ?>

    <div class="page-intro homepage-intro">
      <div class="padding-wrapper">
        <div class="small-wrapper">

          <?php the_field( 'page_intro' ); ?>

        </div>
      </div>
    </div>

  <?php endif; ?>

  <main class="site-main homepage" role="main">

    <div class="padding-wrapper">

      <div class="text-wrapper">

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

          <?php if( !is_front_page() ): ?>

            <h1><?php the_title(); ?></h1>

          <?php endif; ?>

          <div class="page-content">

            <?php the_content(); ?>

          </div>

        <?php endwhile; endif; wp_reset_query(); ?>

      </div>

    </div>

    <?php if( have_rows( 'services' ) ): ?>

      <section class="services">
        <div class="padding-wrapper">
          <div class="medium-wrapper">

            <?php while( have_rows( 'services' ) ): the_row(); ?>

              <div class="service">

                <div class="service-icon">

                  <?php

                    $image = get_sub_field( 'service_icon' );
                    $size = 'icon';
                    $src = $image['url'];
                    $alt = $image['alt'];
                    $thumb = $image['sizes'][ $size ];

                  if( $image ): ?>

                    <img style="max-width: 100px;" src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" />

                  <?php endif; ?>

                </div>

                <h4><?php the_sub_field( 'title' ); ?></h4>

                <div class="description">

                  <?php the_sub_field( 'description' ); ?>

                </div>

              </div>

            <?php endwhile; ?>

          </div>
        </div>
      </section>

    <?php endif; ?>

    <section class="content-block medium-background text-center">
      <div class="padding-wrapper">
        <div class="small-wrapper">

          <h2><?php the_field( 'about_title' ); ?></h2>

          <?php the_field( 'about_paragraph' ); ?>

        </div>
      </div>
    </section>

    <?php if( have_rows( 'work_examples', 'options' ) ): ?>

      <section class="work-examples-section">
        <div class="padding-wrapper">

          <h2><?php the_field( 'work_examples_title' ); ?></h2>

          <div class="work-examples">

            <?php while( have_rows( 'work_examples', 'options' ) ): the_row(); ?>

              <?php if( get_sub_field( 'featured', 'options' ) === true ): ?>

                <div class="work-example">

                  <a href="<?php the_sub_field( 'project_link', 'options' ); ?>">

                    <?php

                      $image = get_sub_field( 'image' );
                      $size = 'medium-flexible';
                      $src = $image['url'];
                      $alt = $image['alt'];
                      $thumb = $image['sizes'][ $size ];

                    if( $image ): ?>

                      <img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" />

                    <?php endif; ?>

                  </a>

                </div>

              <?php endif; ?>

            <?php endwhile; ?>

          </div>
        </div>
      </section>

    <?php endif; ?>

    <div class="cta dark-background">

      <h2><?php the_field( 'cta_headline' ); ?></h2>

      <a class="button button-primary" href="<?php the_field( 'cta_page_link' ); ?>" onclick="ga('send', 'event', 'Google Link', 'Action label', 'Action Value');">
        <?php the_field( 'cta_button_text' ); ?>
      </a>

    </div>

    <section class="page-links-section">
      <div class="padding-wrapper">
        <div class="medium-wrapper">

          <h2><?php the_field( 'page_links_title' ); ?></h2>

          <?php if( have_rows( 'page_links' ) ): ?>

            <div class="page-links">

              <?php while( have_rows( 'page_links' ) ): the_row(); ?>

                <div class="page-link">

                  <?php

                    $image = get_sub_field( 'image' );
                    $size = 'medium-flexible';
                    $src = $image['url'];
                    $alt = $image['alt'];
                    $thumb = $image['sizes'][ $size ];

                  if( $image ): ?>

                    <img style="max-width: 400px;" src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" />

                  <?php endif; ?>

                  <a href="<?php the_sub_field( 'page_link' ); ?>" class="button button-primary">
                    <?php the_sub_field( 'button_text' ); ?>
                  </a>

                </div>

              <?php endwhile; ?>

            </div>

          <?php endif; ?>

        </div>
      </div>
    </section>

  </main>

<?php get_footer(); ?>
