<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta charset="UTF-8">
	<title><?php wp_title(); ?></title>

	<?php wp_head(); ?>

	<meta name="viewport" content="width=device-width, initial-scale=1">
    <!--[if lt IE 9]>
      <script src="https://html5shim.googlecode.com/svn/trunk/html5.js">
      </script>
    <![endif]-->

	<?php if( function_exists( 'get_field' ) ): ?>

		<?php if( get_field( 'google_analytics', 'options' ) ): ?>

			<?php the_field( 'google_analytics', 'options' ); ?>

		<?php endif; ?>

	<?php endif; ?>
</head>
<body <?php body_class(); ?> id="page-top">
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T7CG8R8"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<?php

		function dashItAll($string) {
			//Lower case everything
			$string = strtolower($string);
			//Make alphanumeric (removes all other characters)
			$string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
			//Clean up multiple dashes or whitespaces
			$string = preg_replace("/[\s-]+/", " ", $string);
			//Convert whitespaces and underscore to dash
			$string = preg_replace("/[\s_]/", "-", $string);
			return $string;
		}

	?>

	<?php
	  $banner = wp_get_attachment_image_src( get_field( 'default_banner_image', 'options' ), 'banner' );
	?>

  <header class="page-banner <?php if( is_front_page() ) { echo 'homepage'; } ?>" style="background-image: url(<?php echo $banner[0]; ?>);">

		<div class="header-main">
			<div class="padding-wrapper">

				<div onclick="openNav()" class="toggle-sidebar">
					<div class="hamburger-label">Menu</div>
					<div class="hamburger">
						<span></span>
						<span></span>
						<span></span>
					</div>
				</div>

				<div id="mySidenav" class="sidenav">
					<div class="sidenav-wrapper">

						<div id="sidebar">
							<div class="mobile-nav-header clearfix">
								<div class="close-sidenav clearfix">
									<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">

										<img src="<?php bloginfo( 'template_directory' ); ?>/img/close.svg" />

									</a>

								</div>
							</div>

							<div class="mobile-navigation">

								<nav class="mobile-menu">

									<?php
										$defaults = array(
											'container' => false,
											'div' => false,
											'theme_location' => 'main-menu'
										);
									 wp_nav_menu( $defaults );
									?>

								</nav>

							</div>
						</div>
					</div>
				</div>

				<script>
					/* Set the width of the side navigation to 250px */
					function openNav() {
						jQuery('.sidenav').toggleClass('sidenav-open');
						jQuery('html, body').toggleClass('nav-open');
					}

					/* Set the width of the side navigation to 0 */
					function closeNav() {
						jQuery('.sidenav').toggleClass('sidenav-open');
						jQuery('html, body').toggleClass('nav-open');
					}
				</script>

			</div>
		</div>

		<div class="banner-content">
			<div class="padding-wrapper">
				<div class="small-wrapper">

					<?php if( is_front_page() ): ?>

						<div class="header-logo">

							<?php if( get_custom_logo() ): ?>

								<?php the_custom_logo( 'medium' ); ?>
							    
							<?php else: ?>
								
								<a href="<?php echo esc_url( home_url() ); ?>"><?php bloginfo( 'name' ); ?></a>

							<?php endif; ?>

				    </div>

				   <?php else: ?>

				   	<h1><?php the_title(); ?></h1>

				   <?php endif; ?>
					
				</div>	
			</div>

		</div>

  </header>
