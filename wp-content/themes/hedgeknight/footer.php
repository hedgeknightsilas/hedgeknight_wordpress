  </div><!-- #content -->

<?php

  $background = wp_get_attachment_image_src( get_field( 'footer_image', 'options' ), 'full-page' );

?>

<section class="full-image-background" style="background-image: url(<?php echo $background[0]; ?>);"></section>

<section class="footer__contact">

  <?php

    $image = get_field( 'footer_logo', 'options' );
    $size = 'logo';
    $src = $image['url'];
    $alt = $image['alt'];
    $thumb = $image['sizes'][ $size ];

  if( $image ): ?>

    <img class="footer__logo" style="max-width: 400px;" src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>"/>

  <?php endif; ?>

  <?php the_field( 'footer_info', 'options' ); ?>

</section>

<footer>

  <div class="padding-wrapper">
    <div class="legal">
      <p>© <?php echo date("Y"); ?> <?php bloginfo( 'name' ); ?></p>
    </div>

    <div class="social">
      
      <?php if( have_rows( 'social', 'options' ) ): ?>

        <?php while( have_rows( 'social', 'options' ) ): the_row(); ?>

          <a class="social-icon" href="<?php the_sub_field( 'social_link' ); ?>"><?php the_sub_field( 'social_icon' ); ?></a>

        <?php endwhile; ?>

      <?php endif; ?>

    </div>

  </div>

</footer>

<?php wp_footer(); ?>

</body>
</html>