<?php

// Supports
add_theme_support( 'post-thumbnails');

add_theme_support( 'menus' );

add_theme_support( 'custom-logo' );


// Image sizes
add_image_size( 'hedgeknight-medium', 800, 600, true );

add_image_size( 'medium-flexible', 800, 600 );

add_image_size( 'logo', 400, 400 );

add_image_size( 'full-page', 1920, 600 );


// Register Menus
function register_theme_menus() {
  register_nav_menus(
    array(
      'main-menu' => __( 'Main Menu', 'hedgeknight' ),
      'hamburger-menu' => __( 'Hamburger Menu', 'hedgeknight' )
    )
  );
}
add_action( 'init', 'register_theme_menus');

// Enable Options Tab
if( function_exists('acf_add_options_page') ) {
  acf_add_options_page();
}

// Set up Style References
function hedgeknight_theme_styles(){

  wp_enqueue_style( 'normalize', get_template_directory_uri() . '/css/normalize-min.css');
  wp_enqueue_style( 'main', get_template_directory_uri() . '/style.css');
  wp_enqueue_style( 'fonts', 'https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,700|Vesper+Libre:400,700' );

}
add_action( 'wp_enqueue_scripts', 'hedgeknight_theme_styles' );

// Set up Script References
function hedgeknight_theme_js(){

  wp_enqueue_script( 'FontAwesome', 'https://use.fontawesome.com/releases/v5.0.2/js/all.js', '', '', true );
  wp_enqueue_script( 'FontAwesomeShim', 'https://use.fontawesome.com/releases/v5.0.2/js/v4-shims.js', '', '', true );
  wp_enqueue_script( 'main_js', get_template_directory_uri() . '/js/main-min.js', array('jquery'), '', true );
  wp_enqueue_script( 'hedgeknight_customizer', get_template_directory_uri() . '/js/customize.js', array( 'jquery','customize-preview' ), '',true );
  wp_enqueue_script( 'Viewport Checker', get_template_directory_uri() . '/js/jquery.viewportchecker.min.js', array('jquery'), '', true );


}
add_action('wp_enqueue_scripts', 'hedgeknight_theme_js' );

// Custom Logo
function hedgeknight_custom_logo_setup() {
    $defaults = array(
      'flex-width' => true,
      'header-text' => array( 'site-title', 'site-description' ),
    );
    add_theme_support( 'custom-logo', $defaults );
}
add_action( 'after_setup_theme', 'hedgeknight_custom_logo_setup' );

// Register Widget
function hedgeknight_widgets_init() {
  register_sidebar( array(
    'name'          => __( 'Blog Sidebar', 'hedgeknight' ),
    'id'            => 'blog-sidebar',
    'description'   => __( 'Add widgets here to appear in your sidebar on blog posts and archive pages.', 'hedgeknight' ),
    'before_widget' => '<section id="%1$s" class="widget %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h2 class="widget-title">',
    'after_title'   => '</h2>',
  ) );
}
add_action( 'widgets_init', 'hedgeknight_widgets_init' );


// Add Editor back to default Posts page
function fix_no_editor_on_posts_page($post) {

   if( $post->ID != get_option( 'page_for_posts' ) ) { return; }

   remove_action( 'edit_form_after_title', '_wp_posts_page_notice' );
   add_post_type_support( 'page', 'editor' );

 }

 // This is applied in a namespaced file - so amend this if you're not namespacing
 add_action( 'edit_form_after_title', 'fix_no_editor_on_posts_page', 0 );

 // To set post count format in sidebar
function categories_postcount_filter ($variable) {
  $variable = str_replace('(', '<span class="post-count">', $variable);
  $variable = str_replace(')', ' Posts</span>', $variable);
  return $variable;
}
add_filter('wp_list_categories','categories_postcount_filter');

function archive_postcount_filter ($variable) {
   $variable = str_replace('(', '<span class="post-count">', $variable);
   $variable = str_replace(')', ' Posts</span>', $variable);
   return $variable;
}
add_filter('get_archives_link', 'archive_postcount_filter');

// Handle Page Title
add_action( 'after_setup_theme', 'hedgeknight_theme_setup' );
function hedgeknight_theme_setup() {
    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support( 'title-tag' );
}

// Add default posts and comments RSS feed links to head.
  add_theme_support( 'automatic-feed-links' );


// Set Container Width for Theme
if ( ! isset( $content_width ) )
    $content_width = 980;


/**
 * Apply theme's stylesheet to the visual editor.
 *
 * @uses add_editor_style() Links a stylesheet to visual editor
 * @uses get_stylesheet_uri() Returns URI of theme stylesheet
 */

function hedgeknight_add_editor_styles() {
  add_editor_style( get_stylesheet_uri() );
}
add_action( 'init', 'hedgeknight_add_editor_styles' );


// Remove query strings from static resources

function remove_css_js_ver( $src ) {
if( strpos( $src, '?ver=' ) )
$src = remove_query_arg( 'ver', $src );
return $src;
}
add_filter( 'style_loader_src', 'remove_css_js_ver', 10, 2 );
add_filter( 'script_loader_src', 'remove_css_js_ver', 10, 2 );

function dashItAll($string) {
  //Lower case everything
  $string = strtolower($string);
  //Make alphanumeric (removes all other characters)
  $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
  //Clean up multiple dashes or whitespaces
  $string = preg_replace("/[\s-]+/", " ", $string);
  //Convert whitespaces and underscore to dash
  $string = preg_replace("/[\s_]/", "-", $string);
  return $string;
}