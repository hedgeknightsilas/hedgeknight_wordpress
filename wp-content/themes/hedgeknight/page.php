<?php get_header(); ?>

  <main class="site-main" role="main">
    <div class="padding-wrapper">

      <?php if( have_posts() ): ?>

        <div class="content-area">

          <?php while( have_posts() ): the_post(); ?>

            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

              <div class="text-wrapper">

                <header class="entry-header">

                  <?php if( !is_front_page() ): ?>

                    <h1><?php the_title(); ?></h1>

                  <?php endif; ?>

                </header>

                <div class="entry-content">

                  <?php the_content(); ?>

                  <div class="page-links">

                    <?php if( function_exists( 'wp_pagenavi') ): ?>

                      <div class="navigation">

                        <?php wp_pagenavi(); ?>

                      </div>

                    <?php else: ?>

                      <?php wp_link_pages( array(
                        'before'      => '<div class="page-links">' . __( 'Pages:', 'hawkwood' ),
                        'after'       => '</div>',
                        ) );
                      ?>

                    <?php endif; ?>

                  </div>
                  
                </div>

              </div>

            </article>

          <?php endwhile; ?>

        </div>

      <?php endif; ?>

    </div>
  </main>

<?php get_footer(); ?>
