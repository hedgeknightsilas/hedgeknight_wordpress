<?php if( function_exists( 'get_field' ) ): ?>

  <?php
  global $post;
    $banner = wp_get_attachment_image_src( get_field( 'page_banner' ), 'banner' );
    $defaultBanner = wp_get_attachment_image_src( get_field( 'default_banner_image', 'options' ), 'banner' );
    $posts_page_id = get_option( 'page_for_posts' ); 
    $posts_page = get_post( $posts_page_id );
  ?>

  <?php if( get_field( 'page_banner' ) ): ?>

    <div class="page-banner subpage-banner" style="background-image: url(<?php echo $banner[0]; ?>);"></div>

  <?php elseif( has_post_thumbnail($posts_page_id) ): ?>

    <?php 
      $url = wp_get_attachment_url( get_post_thumbnail_id($posts_page_id), 'medium'); 
    ?>

    <div class="page-banner subpage-banner" style="background-image: url(<?php echo $url; ?>)"></div>

  <?php elseif( get_field( 'default_banner_image', 'options' ) ): ?>

    <div class="page-banner subpage-banner" style="background-image: url(<?php echo $defaultBanner[0]; ?>"></div>

  <?php endif; ?>

<?php endif; ?>