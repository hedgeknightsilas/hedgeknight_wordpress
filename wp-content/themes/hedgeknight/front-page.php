<?php get_header( 'home' ); ?>

  <main class="site-main" role="main">

    <?php if( have_rows( 'quicklinks' ) ): ?>

      <section class="light-background">

        <div class="quicklinks">

          <?php while( have_rows( 'quicklinks' ) ): the_row(); ?>

            <div class="card hidden">

              <div class="card__icon">

                <?php

                  $image = get_sub_field( 'icon' );
                  $size = 'logo';
                  $src = $image['url'];
                  $alt = $image['alt'];
                  $thumb = $image['sizes'][ $size ];

                if( $image ): ?>

                  <img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>"/>

                <?php endif; ?>

              </div>

              <div class="card__heading">

                <p class="card__heading--underlined"><?php the_sub_field( 'card_heading' ); ?></p>

              </div>

              <div class="card__body">

                <div class="card__text">

                  <?php the_sub_field( 'text' ); ?>

                </div>

              </div>

            </div>

          <?php endwhile; ?>

        </div>

      </section>

    <?php endif; ?>

    <?php if( have_posts() ): ?>

      <section class="content-block dark-background text-center">
        <div class="padding-wrapper">
          <div class="small-wrapper">

            <?php while( have_posts() ): the_post(); ?>

              <div>

                <?php the_content(); ?>

              </div>

            <?php endwhile; ?>

          </div>

        </div>

      </section>

    <?php endif; wp_reset_query(); ?>

    <?php if( have_rows( 'pathways' ) ): ?>

      <section class="pathways">

        <?php $count = 0; ?>

        <?php while( have_rows( 'pathways' ) ): the_row(); ?>

          <?php
            $background = wp_get_attachment_image_src( get_sub_field( 'background' ), 'full-page' );
          ?>

          <div class="pathways__item content-block" style="background-image: url(<?php echo $background[0]; ?>);">
            <div class="flex-wrapper">

              <div class="pathways__block pathways__header">

                <h3 class="pathways__heading"><?php the_sub_field( 'heading' ); ?></h3>
                
                <div class="pathways__button">
                  <a class="button button--accent" href="<?php the_sub_field( 'page_link' ); ?>"><?php the_sub_field( 'button_text' ); ?></a>
                </div>

              </div>

              <div class="pathways__block pathways__description">

                <?php the_sub_field( 'text' ); ?>

              </div>

            </div>

          </div>

          <?php if( $count < 1): ?>

            <div class="divider">Or</div>

            <?php $count++; ?>

          <?php endif; ?>

        <?php endwhile; ?>

      </section>

    <?php endif; ?>

    <section class="content-block text-block light-background">
      <div class="padding-wrapper">
        <div class="text-wrapper">

          <?php the_field( 'text_block' ); ?>

        </div>
      </div>
    </section>

  </main>

<?php get_footer(); ?>
